#how to install jenkins using docker

```
docker-compose pull
docker-compose up -d
```

For local intstallation visit URL

```
http://localhost:8080
```

For Remote installation visit URL (be sure that you open port 8080 for public access)

```
http://remote-ip:8080
```